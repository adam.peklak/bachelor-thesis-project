import { UPDATE_TASKS } from "./types"
import ApiCalendar from "react-google-calendar-api"
import store from "../store"

// Fetches tasks from Google Calendar
export const fetchTasks = (amount = 10) => dispatch => {
  ApiCalendar.listUpcomingEvents(amount)
    .then(({result}) => {
      let tasks = result.items.map(t => {
        return {
          id: t.id,
          created: t.created,
          updated: t.updated,
          start: t.start,
          end: t.end,
          summary: t.summary,
          status: t.status,
          location: t.location,
          author: t.creator.email,
          sector: 0
        }
      })

      dispatch({
        type: UPDATE_TASKS,
        payload: tasks
      })
    });
}

// Assigns a task sector 1..4
// if sector == 0 (default), task is unAssigned from any sector
export const putTaskInSector = (task_id, sector) => dispatch => {
  const state = store.getState()
  let tasks_copy = Object.assign([], state.tasks.tasks)

  if (task_id !== undefined && tasks_copy.length > 0) {
    tasks_copy.find(t => t.id === task_id).sector = sector
    dispatch({
      type: UPDATE_TASKS,
      payload: tasks_copy
    })
  }
}