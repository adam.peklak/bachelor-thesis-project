export function formatDateTime(start, end) {
  const noTimeSpecified = !(typeof start.dateTime === 'string' || typeof end.dateTime === 'string')
  let complete_str = ""

  const s = new Date(noTimeSpecified ? start.date : start.dateTime)
  let start_string = `${s.getDate()}. ${s.getMonth()+1}.`
  const e = new Date(noTimeSpecified ? end.date : end.dateTime)
  let end_string = `${e.getDate()}. ${e.getMonth()+1}.`
  const time_str = `${s.getHours()}:${s.getMinutes() === 0 ? '00' : s.getMinutes()} 
                  - ${e.getHours()}:${s.getMinutes() === 0 ? '00' : s.getMinutes()}`
  const isSingleDay = start_string === end_string

  if (noTimeSpecified) {
    if (isSingleDay) {
      complete_str = start_string
    } else {
      complete_str = start_string + " - " + end_string
    }
  } else {
    if (isSingleDay) {
      complete_str = start_string + " " + time_str
    } else {
      complete_str = start_string + ` ${s.getHours()}:${s.getMinutes() === 0 ? '00' : s.getMinutes()} 
                 - ` + end_string + ` ${e.getHours()}:${s.getMinutes() === 0 ? '00' : s.getMinutes()}`
    }
  }

  return complete_str
}