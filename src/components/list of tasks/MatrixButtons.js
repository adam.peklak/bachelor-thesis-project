import React, { Component } from 'react'

class MatrixButtons extends Component {

  constructor(props) {
    super(props)
    this.putTaskInSector = this.putTaskInSector.bind(this)
  }

  putTaskInSector(task_id, sector) {
    this.props.putTaskInSector(task_id,
      sector === this.props.task.sector ? 0 : sector)
  }

  render() {

    const items = [...Array(4)].map((q, i) => (
      <div key={i} className={`quadrant q${i+1} ` + (this.props.task.sector === i+1 ? "active" : "")}
           onClick={() => this.putTaskInSector(this.props.task.id, i+1)}>
      </div>
    ))

    return (
      <div className="matrix-buttons">
        { items }
      </div>
    )
  }
}

export default MatrixButtons