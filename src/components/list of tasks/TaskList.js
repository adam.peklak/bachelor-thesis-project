import React, { Component } from 'react'
import { connect } from 'react-redux'
import { fetchTasks, putTaskInSector } from "../../redux/actions/taskActions"
import TaskItem from "./TaskItem"
import ApiCalendar from "react-google-calendar-api"

class TaskList extends Component {

  constructor(props) {
    super(props)
    ApiCalendar.onLoad(() => this.props.fetchTasks(10))
  }

  render() {
    const taskItems = this.props.tasks
      .sort((a, b) => a.start - b.start)
      .sort((a, b) => a.sector - b.sector)
      .map(task =>
      <TaskItem task={task}
                key={task.id}
                putTaskInSector={this.props.putTaskInSector} />)

    return (
      <div id="task-list">
        <header>
          <h2>List of tasks</h2>
        </header>
        <div className="tasks-container">
          { taskItems }
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  tasks: state.tasks.tasks
})

export default connect(mapStateToProps, { fetchTasks, putTaskInSector })(TaskList)