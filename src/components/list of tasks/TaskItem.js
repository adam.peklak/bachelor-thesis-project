import React from 'react'
import { formatDateTime } from "../../helpers/date"
import MatrixButtons from "./MatrixButtons"

const TaskItem = (props) => {
  return (
      <div className={"task " + (props.task.sector === 0 ? "unassigned" : "assigned")}>
        <div className="left">
          <div className="description">{props.task.summary}</div>
          <div className="date">{formatDateTime(props.task.start, props.task.end)}</div>
        </div>
        <div className="right">
          <MatrixButtons task={props.task} putTaskInSector={props.putTaskInSector} />
        </div>
        <div className="clear"></div>
      </div>
    )
}
export default TaskItem