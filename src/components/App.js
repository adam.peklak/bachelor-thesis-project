import React, { Component } from 'react'
import '../assets/stylesheets/application.scss'
import { Provider } from 'react-redux'
import store from '../redux/store'
import TaskList from "./list of tasks/TaskList"
import Matrix from "./matrix/Matrix"
import domtoimage from 'dom-to-image'
import FileSaver from 'file-saver'

export default class App extends Component {

  constructor(props) {
    super(props)
    this.downloadImage = this.downloadImage.bind(this)
  }

  downloadImage = () => {
    domtoimage.toBlob(document.getElementById('matrix'))
      .then(function (blob) {
        FileSaver.saveAs(blob, 'Matrix.png')
      });
  }

  render() {
    return (
      <Provider store={store}>
        <main id="grid">

          <header>
            <h1>Eisenhower Matrix Task Organizer</h1>
            <p>This app can help you organize your tasks into 4 categories depending on their urgency & importance.</p>
          </header>

          <TaskList/>
          <button id="download" onClick={this.downloadImage}>Download Image</button>

          <Matrix/>

          <footer>
            <span>Adam Peklák - Practical part of Bachelor Thesis</span>
          </footer>
        </main>
      </Provider>
    );
  }
}
