import React, { Component } from 'react'
import { connect } from 'react-redux'
import Sector from "./Sector"
import { putTaskInSector } from "../../redux/actions/taskActions"

class Matrix extends Component {

  constructor(props) {
    super(props)
    this.getTaskItems = this.getTaskItems.bind(this)
    this.unAssignTask = this.unAssignTask.bind(this)
  }

  getTaskItems(sector = 0) {
    return sector === 0 ? this.props.tasks : this.props.tasks.filter(task => task.sector === sector)
  }

  unAssignTask(task_id) {
    this.props.putTaskInSector(task_id, 0)
  }

  render() {
    const sectors = ["DO NOW", "DO LATER", "DELEGATE", "DON'T DO"]

    const items = sectors.map((s, i) =>
      <Sector key={i}
              sectorNumber={i+1}
              bgText={s}
              tasks={this.getTaskItems(i+1)}
              onDelete={this.unAssignTask} />)

    return (
      <div id="matrix">
        { items }
      </div>
    )
  }
}

const mapStateToProps = state => ({
  tasks: state.tasks.tasks
})

export default connect(mapStateToProps, { putTaskInSector })(Matrix)