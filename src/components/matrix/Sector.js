import React from 'react'
import Task from "./Task"

const Sector = (props) => {
  const numberOfAssignedTasks = props.tasks.filter(t => t.sector === props.sectorNumber).length

  const taskItems = props.tasks.map((task, index) => (
    <Task key={index}
          task={task}
          onDelete={props.onDelete}
          manyTasks={numberOfAssignedTasks > 4} />
  ))

  return (
    <div className={"quadrant q" + props.sectorNumber + (numberOfAssignedTasks > 4 ? " many-tasks" : "")}>
      <div className="background-text">{props.bgText}</div>
      { taskItems }
    </div>
  )
}
export default Sector
