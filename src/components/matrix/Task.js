import React from 'react'
import { formatDateTime } from "../../helpers/date"

const Task = (props) => {

  const summary = props.task.summary == null ? null :
    <div className="description">{ props.task.summary }</div>

  const location = props.task.location == null ? null :
    <div className="location">
      <i className="fa fa-map-marker" aria-hidden="true"></i>&nbsp;
      { props.task.location }
    </div>

  const date = props.task.start == null ? null :
    <div className="date">
      <i className="fa fa-calendar" aria-hidden="true"></i>&nbsp;
      { formatDateTime(props.task.start, props.task.end) }
    </div>

  const author = props.task.author == null ? null :
    <div className="author">
      <i className="fa fa-user-tie" aria-hidden="true"></i>&nbsp;
      { props.task.author }
    </div>

  return (
    <div className="task">
      { summary }
      { date }
      <div className="additional-info" style={props.manyTasks ? {display: "none"} : {display: "block"}}>
        { location }
        { author }
      </div>
      <span onClick={() => props.onDelete(props.task.id)} className="delete" title="Delete Task"></span>
    </div>
  )
}
export default Task
